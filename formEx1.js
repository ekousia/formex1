document.getElementById("submit").addEventListener("click", function (event) {
  console.log("from click button", event);
  // validateForm(event); // και εδω μπορουμε να καλεσουμε κατευθειαν την validate
  // event.preventDefault(); // και εδω μπορουμε να σταματησουμε το event propagation
});

document.getElementById("myform").addEventListener("submit", function (event) {
  console.log("from form submit", event);
  validateForm(event);
  event.preventDefault();
});

//eye button event-listener
var eye = document.querySelector(".eye");

if (eye) {
  eye.addEventListener("click", function () {
    eye.classList.toggle("active");
    var pwd = document.querySelector("input[id='password']");
    if (pwd.getAttribute("type") === "password") {
      pwd.setAttribute("type", "text");
    } else {
      pwd.setAttribute("type", "password");
    }
  });
} else {
  console.log("eye not found");
}

//reset button event-listener
var resetButton = document.querySelector(".btn_reset");

if (resetButton) {
  resetButton.addEventListener("mouseover", function () {
    resetButton.style.cursor = "pointer";
  });
  resetButton.addEventListener("focus", function () {
    resetButton.style.cursor = "progress";
  });
} else {
  console.log("reset not found");
}


document.getElementById("username").addEventListener("keyup", function() {
  document.getElementById("usr").innerHTML = document.getElementById("username").value;
})

document.getElementById("password").addEventListener("keyup", function() {
  document.getElementById("pass").innerHTML = document.getElementById("password").value;
})

document.querySelector("input[type=radio][id=male]").addEventListener("click", function() {
  document.getElementById("gndr").innerHTML = "male";
})

document.querySelector("input[type=radio][id=female]").addEventListener("click", function() {
  document.getElementById("gndr").innerHTML = "female";
})

var checkbox = document.querySelector("input[name=agree]");

checkbox.addEventListener("click", function() {
    if(this.checked) {
      document.getElementById("agree_span").innerHTML = "I agree";
    }
    else{
      document.getElementById("agree_span").innerHTML = " ";
    } 
});

var falseP = true;
document.getElementById("username").addEventListener("blur", function(event){
  var usernameInput = document.getElementById("username").value;
  var pattern = /^[0-9a-zA-Z_.-]{5,10}/;
  if (! pattern.test(usernameInput)) {
    if (falseP){
      var falseInputP = document.createElement("p");
      falseInputP.className = "false_input_p";
      falseInputP.innerHTML = "Invalid username. Please enter a valid one!";
      document.getElementById("username_container").insertAdjacentElement("afterend", falseInputP);
      falseP = false;
    }
  } else{
    if (! falseP){
      [...document.getElementsByClassName("false_input_p")].map(n => n && n.remove());
      falseP = true
    }
  }
});
